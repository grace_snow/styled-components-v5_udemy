import React from 'react';
// import {Button} from 'components/common';
import {createGlobalStyle} from 'styled-components';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Home from 'components/pages/Home';
import Login from 'components/pages/Login';

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
  }

  body {
    background: #fff;
    color: #000;
    margin: 0;
    min-height: 100vh;
    font-family: 'Kaushan Script', sans-serif;
  }
`;

function App() {
  return (
    <div>
      <GlobalStyle />
      <BrowserRouter>
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
