import React, { useState } from 'react';
import { PageLayout, Input, PasswordInput, Label } from 'components/common';
import styled from 'styled-components';

const Form = styled.form`
  width: 100%;
  max-width: 400px;
  padding: 1rem;
  background: #fff;
  color: #000;
  border: 1px solid #eee;
  border-radius: 4px;
`;

export default function Login() {
  const [formFields, setFormFields] = useState({username: '', password: ''});
  
  function handleInputChange(e) {
    e.persist();
    setFormFields(state => ({
      ...state,
      [e.target.name]: e.target.value
    }));
  }

  return (
    <PageLayout>
      <h1>
        Login
      </h1>
      <Form>
        <Label htmlFor="username">Username:</Label>
        <Input 
          value={ formFields.username }
          onChange={ handleInputChange }
          name="username"
          type="text"
        />
        <label htmlFor="password">
          Password:
          <PasswordInput 
            value={ formFields.password }
            onChange={ handleInputChange }
            name="password"
          />
        </label>
      </Form>
    </PageLayout>
  )
}