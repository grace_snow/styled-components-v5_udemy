import styled, { css } from 'styled-components';

// button here is the html element you want to output.
const Button = styled.button` 
	color: white;
	background: ${ props => props.secondary ? '#000' : '#f8049c' };
	font-weight: bold;
	box-shadow: none;
	border: none;
	white-space: none;
	width: 100%;
	display: block;

	${ p => p.large ? css`
		padding: 10px;
		border-radius: 5px;
		font-size: 1.5em;
	` : css`
		padding: 8px;
		border-radius: 4px;
		font-size: 1em;
	`}

	&:disabled {
		background: #eee;
		color: #666;
	}
`;

export {Button};


// NOTE: alternative way to define multiple style attrubutes based off props
// Could also use a switch statement to be neater here:
/* ${props => {
	if (props.secondary) {
		return `
			background: ${colors.dark};
			color: ${colors.light};
	`
	} else if (props.tertiary) {
		return ` 
			background: ${colors.light};
			color: ${colors.dark};
	`
	} else {
		return `
			background: ${colors.light};
			color: ${colors.dark};
	`
	}
}} */