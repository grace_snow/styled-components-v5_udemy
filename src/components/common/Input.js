import styled from 'styled-components';

const Input = styled.input`
  width: 100%;
  height: 40px;
  padding: 0.25em 0.5em;
  box-shadow: 0;
  border: 2px solid #bbb;
  border-radius: 4px;
  background: #fff;
  color: #333;
  font-family: 'Open Sans', sans-serif;
  font-size: 1em;
  margin-bottom: 0.5rem;
`;

export { Input };