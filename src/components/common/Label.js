import React from 'react';
import styled from 'styled-components';

const StyledLabel = styled.label`
  font-size: 0.9em;
  font-family: 'Kaushan Script', sans-serif;
`;

export function Label({children, ...props}){
  return (
    <StyledLabel { ...props }>
      { children }
    </StyledLabel>
  )
}