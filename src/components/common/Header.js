import React, { useState } from 'react';
import styled from 'styled-components';
import { Link as RRDlink, useLocation } from 'react-router-dom'; // see below

const HeaderWrapper = styled.header`
  height: 60px;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 16px;
  position: fixed;
  top: 0;
  font-family: 'Open Sans', sans-serif;
  background-image: linear-gradient(to right, #f8049c, #fdd54f);
  border-bottom: 3px solid #fdd54f;
`;

const SiteNav = styled.nav`
  display: ${props => props.open ? 'block' : 'none'};
  position: absolute;
  width: 100%;
  height: auto;
  top: 60px;
  left: 0;
  padding: 0.5rem;
  background: #fff;
  border-bottom: 3px solid #fdd54f;

  @media screen and (min-width: 768px) {
    height: 100%;
    flex-grow:1;
    display: flex;
    justify-content: flex-end;
    align-items: inherit;
    position: relative;
    width: initial;
    top: initial;
    left: initial;
    padding: initial;
    background: initial;
    border-bottom: initial;
  }
`;

// Demonstrating how to add a custom property to an imported default react component
const Link = ({ isActive, children, ...props }) => {
  return (
    <RRDlink { ...props}>
      { children }
    </RRDlink>
  )
}

const SiteNavLink = styled(Link)`
  display: inherit;
  align-items: inherit;
  height: inherit;
  margin-left: 0.5rem;
  padding: 0.5em;
  text-align: center;
  font-weight: ${ props => props.isActive ? '600' : '400' };
  color: black;
  background: transparent;
  transition: all 0.1s ease-in;

  &:hover {
    background: purple;
    color: white;
  }
`;

const MenuIcon = styled.button`
  margin: auto 0 auto auto;
  width: 30px;
  padding: 5px;
  background: transparent;
  border: 0;
  box-shadow: none;

  > div {
    height: 3px;
    background: #000;
    margin: 5px 0;
  }

  &:focus > div,
  &:hover > div {
    background: purple;
  }

  @media screen and (min-width: 768px) {
    display: none;
  }
`;

export function Header() {
  const { pathname } = useLocation();
  const [ menuOpen, setMenuOpen ] = useState(false);
  
  return (
    <HeaderWrapper>
      <Link to="/">
        <img src="" alt="logo" className="logo" />
      </Link> 
      <MenuIcon onClick={ () => setMenuOpen(s => !s) } aria-label="Main Menu">
        <div />
        <div />
        <div />
      </MenuIcon>
      <SiteNav open={ menuOpen }>
        <SiteNavLink to="/" isActive={pathname === '/'}>
          Home
        </SiteNavLink>
        <SiteNavLink to="/login" isActive={pathname === '/login'}>
          Login
        </SiteNavLink>  
      </SiteNav>
    </HeaderWrapper>
  )
}