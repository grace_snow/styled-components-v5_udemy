import React, {useState} from 'react';
import styled from 'styled-components';
import { Input } from './Input';

const PasswordInputWrapper = styled.div`
  display: flex;

  ~ div {
    margin-bottom: 0.5rem;
  }
`;

const ToggleButton = styled.button`
  width: auto;
  padding: 0.5em;
  height: 40px;
  background: #fff;
  border: 2px solid #bbb;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  font-family: 'Open Sans', sans-serif;
  font-size: 0.9em;
  font-weight: 600;
  user-select: none;

  &:focus {
  	outline: 3px solid #f8049c;
	  outline-offset: -5px;
  }
`;

const PasswordInputStyled = styled(Input).attrs(() => ({
  type: 'password'
}))`
  border-right: 0;
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
`;

export function PasswordInput(props) {
  const [showPassword, setShowPassword] = useState(false)

  function handleToggle(e) {
    e.preventDefault();
    setShowPassword(state => !state)
  }

  return (
    <>
      <PasswordInputWrapper>
        <PasswordInputStyled {...props} />
        <ToggleButton onClick={ handleToggle } >
          { showPassword ? 'Hide' : 'Show' }
        </ToggleButton>
      </PasswordInputWrapper>
      <div>{ showPassword ? props.value : '' }</div>
    </>
  )
}