This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

It follows the udemy course [styled-components-v5](https://www.udemy.com/course/react-styled-components/)

**[Course Code Repo](https://github.com/tomphill/styled-components-v5)**